const unleash = require('unleash-server');
const cors = require('cors')
const server = unleash
  .start({
    databaseUrl: 'postgres://postgres:postgres@localhost:5432/unleash',
    port: 4242,
  })
  .then(unleash => {
    unleash.app.use(cors())
    console.log(
      `Unleash started on http://localhost:${unleash.app.get('port')}`,
    );
  });